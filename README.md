[TOC]


# Overview
***
This document describes the Donde mobile SDK for iOS. It is intended for iOS developers who want to integrate the Donde products into their code. The document describes the main Donde interface functions, and provides code examples and snippets.

The SDK manages the communication between your commerce app and Donde via the Donde Search API, and comprises the UI for the Visual Search Widget and for the Similar Items Widget. The SDK provides methods for processing incoming and outgoing requests, configuration options and for handling errors.


# Setting Up Donde

You need to set the SDK to the relevant `app_key` and change it if needed.

## Initialization

The `Donde.start(withKey: appId:)` method triggers the init flow. This must be done at `AppDelegate.didFinishLaunching(_:_:)`:

```swift
func application(_ application: UIApplication,
                didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
                // init Donde SDK with App initialization
        Donde.start(withKey: "app_key", appId: "app_id")
        return true
}

```

Remember to change the `app_key` and `app_id` if the user changes countries.

Optionally you can specify the `variation_id`:

```swift
func application(_ application: UIApplication,
                didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
                // init Donde SDK with App initialization
        Donde.start(withKey: "app_key", appId: "app_id", variationId: "variation_id")
        return true
}

```

If using A/B testing you can access `isDondeEnabled()` after initialization to see if the user should have access to the Donde SDK

```swift
class func isDondeEnabled() -> Bool

```


# Visual Search View
***
The Visual Search View Widget comprises a UI allowing the user to search for products via visual features, and functionality.
VSV lets the user trigger a search based on the current state of the UI.

The widget triggers a call to the Donde search API which returns the relevant products based on their visual attributes.
When given the visual search elements in addition to the filter and sort factors the search API will subsequently return the relevant product IDs.

## Init Options

The Visual Search View provides several customization options which represented within the DDInitOptions class.

You can specify:

* `userId` - The unique user identifier
* `isCompactView` - The flag to show the view in a compact mode
* `showSortAndFilterBar` - The flag to show the sort and filter bars
* `initialSearch` - Optional. Enabling the deep-linking to a certain search
* `lockCategoryOptions` - Optional. Directs the widget on what categories to show, and with what navigation options
* `webCategory` - Optional. Specifies the Multi Domain Category, e.g: "**Denim**"
* `selectedLabels` - Optional. Specifies the answers to be selected when widget loads in a format: `["color:green", "color:red"]`
* `selectedQuestion` - Optional. Specifies the question that will be displayed on widget load
* `selectedDomain` - Optional. Specifies the main question that will be displayed on widget load

## Obtaining the Initial Category

This is an optional step for users who want to initialize the SDK with a locked category.
```swift
let categories = Donde.avialableCategories()
        // from the array find the relevant category to the user selection
let category = categories?.first(where: { $0.name.contains(currentCategory) })
let lockedCategoryOptions = DDLockCategoryOptions(category: category, navigation: NavigationFlag.Hide)
```


## Creating the Visual Search View Widget

See the example app in this [repository](https://bitbucket.org/dondefashion/dondevisualsearchsampleapp/src/master/) for a working example.

In your ViewController implement the `VisualSearchViewDelegate` method `searchChanged()` in your ViewController.
This method is called whenever the user requests a new search.

```swift
func searchChanged() {
    // This method is always a new search
}
```

Initialize the Donde Visual Search using the delegate and selected DDInitOptions:

```swift
let visualSearchView  = Donde.visualSearchEnabledExperiments(with: self, initOptions: initOptions)
```

To use the Multi Domain Category set the `DDInitOptions::webCategory` parameter.

Check that the user is a Donde user and not a control user by looking at the `.result` parameter:

```swift
switch visualSearchView.result {
        case .donde:
            // add view to hierarchy, set height and width constraints
        case .control:
            // handle regular flow
}
```

## Performing a Search

Generate a `resultsHandler`:

```swift
typealias SearchResultCompletionBlock = (_ isNew: Bool,
    _ length: Int,
    _ total: Int,
    _ hasMore: Bool,
    _ items: [DDItem]?,
    _ rawData: [String : Any]?, _ error: DondeError) -> ()
    
let completionBlock: SearchResultCompletionBlock = {
    // handle the new search results
}

    // Pass it to the Donde SDK
Donde.setSearchCompletionBlock(completionBlock)
```

From now on, new results will be handed to the `completionBlock`.
The items will come in as `DDItem` with all the info you need to display the PLP and PDP.

To perform a search generate SearchParameters object:

```swift
let params: SearchParameters = SearchParameters(filters: nil,
    paginationOffset: 0, // page * numberOfItems
    paginationLimit: 15, // numberOfItems
    Sizes: nil,
    Brands: nil,
    maxPrice: nil,
    minPrice: nil,
    Sort: nil)

Donde.performSearch(with: params)
```

# Similar Items View
***
The similar items view is integrated into the PDP. You can have Donde render all elements of the `SIV` or render them yourself.

## Creating the Similar Items View

Implement the `SimilarItemsControllerDelegate` in the relevant ViewController.
This will give you access to the two methods you need:

```swift
// this method is used to hide spinners when the widget is done
func similarItemsController(_ similarItemsController: SimilarItemsController,
didFinishLoadingWidgetWith error: DondeError) {
    // Handle the PDP events
}

// this method tells you the user has selected another item and you need to open another PDP
func similarItemsController(_ similarItemsController: SimilarItemsController, 
didSelect item: DDItem)
```

Generate a `DondeInitSimilarItemsWithExperiments` object:

```swift
// the type can be .extraItems for complete the look or .mainItem for similar items
let itemsOptions = DondeSimilarItemsViewOptions(viewType: ViewType.mainItem,
    displayFeatures: true, 
    displayItems: true) // The two booleans let you control who displays the items
 ```   
Generate a DondeSimilarItemsViewOptions  object and pass it the options: 

```swift
let similarItemsView = Donde.similarItemsControllerEnabledExperiments(with: self,
    itemId: productID,
    userId: userID,
    viewOptions: itemsOptions)
```

Check the .result to see if the user is a Donde or control user -

```swift
switch similarItemsView.result {
        case .donde:
            // Add view to hierarchy, set height and width constraints
        case .control:
            // handle regular flow
}
```

# Support
Feedback and inquires can be sent to [Donde](support@dondesearch.com).
